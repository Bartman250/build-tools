FROM amazonlinux
MAINTAINER @dazraf

ARG JAVA_VERSION=1.8.0
ARG MAVEN_VERSION=3.6.1
ARG DOCKER_CHANNEL=stable
ARG DOCKER_VERSION=18.09.6

LABEL   name="dazraf/build-tools" \
        description="OpenJDK $JAVA_VERSION Maven $MAVEN_VERSION NodeJS 10 and Docker in Docker!" \
        maintainer="dazraf" \
        url="https://gitlab.com/dazraf/build-tools"

RUN	yum update -y && \
	yum install -y git bash wget tar ca-certificates less gzip python3 python-pip hostname && \
	wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64 && \
	chmod +x /usr/local/bin/gitlab-runner

RUN set -eux; \
	\
	apkArch="$(arch)"; \
	case "$apkArch" in \
		x86_64) dockerArch='x86_64' ;; \
		armhf) dockerArch='armel' ;; \
		aarch64) dockerArch='aarch64' ;; \
		ppc64le) dockerArch='ppc64le' ;; \
		s390x) dockerArch='s390x' ;; \
		*) echo >&2 "error: unsupported architecture ($apkArch)"; exit 1 ;;\
	esac; \
	\
	if ! wget -O docker.tgz "https://download.docker.com/linux/static/${DOCKER_CHANNEL}/${dockerArch}/docker-${DOCKER_VERSION}.tgz"; then \
		echo >&2 "error: failed to download 'docker-${DOCKER_VERSION}' from '${DOCKER_CHANNEL}' for '${dockerArch}'"; \
		exit 1; \
	fi; \
	\
	tar --extract \
		--file docker.tgz \
		--strip-components 1 \
		--directory /usr/local/bin/ \
	; \
	rm docker.tgz; \
	\
	dockerd --version; \
	docker --version

COPY modprobe.sh /usr/local/bin/modprobe
COPY docker-entrypoint.sh /usr/local/bin/
COPY gitlab-runner-run.sh /usr/local/bin/gitlab-runner-run

RUN pip install docker-compose && \
	curl -sL https://rpm.nodesource.com/setup_10.x | bash - && \
    yum install -y java-$JAVA_VERSION-openjdk java-$JAVA_VERSION-openjdk-devel nodejs && \
    curl -fsSL https://archive.apache.org/dist/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz | tar xzf - -C /usr/share && \
    mv /usr/share/apache-maven-$MAVEN_VERSION /usr/share/maven && \
    ln -s /usr/share/maven/bin/mvn /usr/bin/mvn && \
	set -eux && \ 
    useradd --create-home --no-log-init --shell /bin/bash gitlab-runner \
    && echo 'gitlab-runner:gitlab-runner' | chpasswd \
    && groupadd docker \
    && usermod -aG docker gitlab-runner \
    && usermod -aG root gitlab-runner && \
	touch /var/run/docker.sock && \
	chmod 666 /var/run/docker.sock && \
	yum clean all  && \
	rm -rf /var/cache/yum

ENV JAVA_HOME /usr/lib/jvm/java
ENV MAVEN_HOME /usr/share/maven

WORKDIR /home/gitlab-runner

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["sh"]





