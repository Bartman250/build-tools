#! /bin/bash

GITLAB_URL=$1
GITLAB_TOKEN=$2

set -e

# pre-req
apt-get update
apt-get install -y curl apt-transport-https ca-certificates
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | bash
curl -sL https://deb.nodesource.com/setup_10.x | bash -
apt-get update

# install packages
apt-get install -y \
    gitlab-runner \
    docker-ce \
    docker-ce-cli \
    containerd.io \
    docker-compose \
    openjdk-8-jdk \
    maven \
    nodejs

# post install config
update-java-alternatives --set java-1.8.0-openjdk-amd64
echo '{ "registry-mirrors": ["https://mirror.gcr.io"]}' > /etc/docker/daemon.json
service docker restart
docker system info
chmod 666 /var/run/docker.sock
usermod -aG docker gitlab-runner

# gitlab-runner registration
gitlab-runner \
    register -n \
    --name "${HOSTNAME}-shell" \
    --executor shell \
    --shell bash \
    --limit 1 \
    --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
    --docker-volumes /home/gitlab-runner/builds:/builds \
    --url $GITLAB_URL \
    --registration-token $GITLAB_TOKEN \
    --tag-list shell,docker-compose \
    --run-untagged="false" \
    --locked="false"
gitlab-runner \
    register -n \
    --name "${HOSTNAME}-docker" \
    --executor docker \
    --limit 4 \
    --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
    --docker-image dazraf/build-tools:latest \
    --url $GITLAB_URL \
    --registration-token $GITLAB_TOKEN \
    --tag-list docker \
    --run-untagged="false" \
    --locked="false"
gitlab-runner start
